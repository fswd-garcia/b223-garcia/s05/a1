<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>S05: Client-Server communications (Basic To-do Task list)</title>
</head>
<body>

	<!-- 
		Session
			- A session is a way to store
	 -->

	<?php session_start(); ?>

	<!-- <?php $_SESSION['greet'] = 'Hello'; ?> -->

	<h3>Add Task</h3>

	<!-- "ACTION attribute" contains the destination/URL endpoint -->
	<!-- Create Task -->
	<form method="POST" action="./server.php">
		<!-- ?This will identify the action or type of request of a user -->
		<input type="hidden" name="action" value="add" />
		Description: <input type="text" name="description" required />
		<button type="submit">ADD</button>

	</form>

	<!-- activity start -->

		<form method="POST" action="./server.php">
		<!-- ?This will identify the action or type of request of a user -->
		<br><input type="hidden" name="action" value="email" />
		Email: <input type="email" name="emailAdd" required />
		<button type="submit">Login</button>

		<?php echo $email; ?>

	</form>

	<!-- activity end -->

	<!-- <?php var_dump($_SESSION['tasks']); ?> -->

	<h3>Task List</h3>

    <?php if(isset($_SESSION['tasks'])):  ?>
    	<!-- 
				- $index represents the index number or id of the $_SESSION['tasks']
				- $task reprensents the exact object stored in the $_SESSION['tasks'].
			-->


        <?php foreach($_SESSION['tasks'] as $index=> $task): ?>
            <div>
            	<!-- Update Task -->
					<!-- 
						The following property can be updated in the task:
						- description
						- status (complete/not)
					 -->

                <form method="POST" action="./server.php" style="display: inline-block;">
                    <input type="hidden" name="action" value="update" />
                    <input type="hidden" name="id" value="<?php echo $index; ?>" />
                    <input type="checkbox" name="isFinished" <?php echo ($task->isFinished) ? 'checked' : null; ?> />
                    <!-- This will display the actual task  -->
                    <input type="text" name="description" value="<?php echo $task->description; ?>" />
                    <input type="submit" name="Update" />
                </form>


                <!-- DELETE A TASK -->
                <form method="POST" action="./server.php" style="display: inline-block;" >
                    <input type="hidden" name="action" value="remove" />
                    <input type="hidden" name="id" value="<?php echo $index; ?>" />
                    <input type="submit" value="Delete" />
                </form>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

	

	<br><br>
	<!-- Clear all the task -->
	<form method="POST" action="./server.php">
		<input type="hidden" name="action" value="clear" />
		<button type="submit">CLEAR ALL TASKS</button>


	</form>

</body>
</html>